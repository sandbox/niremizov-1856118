<?php
/**
 * One line Calendar - Style class, extends calendar_plugin_style.
 * This calss overrides render and motnth build function's for enabling rendering
 * of grouping views. It also makes calendar to render in one row, if grouping isn't enabled.
 * This class is working only for month view.
 * 
 * @see calendar_plugin_style
 *
 */
class onelinecal_plugin_style extends calendar_plugin_style {

  var $weekcount; // Variable that stores number of weeks in representing month
  var $datebox; //Variable that stores dates of month. They will be placed inside header inside render() function.

  /**
   * Use parent form, but with some limits.
   * Add onelincal - DataTables properties.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $field_labels = $this->display->handler->get_field_labels(TRUE);
    //Disable mini calendar
    $form['mini']['#disabled'] = TRUE;
    $form['mini']['#default_value'] = 0;
    //Disable all calendar types, except Month.
    $form['calendar_type']['#default_value'] = $this->options['calendar_type'];
    $form['calendar_type']['#options'] = array('month' => t('Month'));
    //Add Onelinecal-DataTables properties
    $options = $this->options['onelinecal'];
    //Each onelinecal propertie is enabled only if grouping is turned on.
    $form['onelinecal'] = array (
      '#type' => 'fieldset',
      '#title' => 'One Line Cal settings',
      '#dependency' => array(
        'edit-style-options-grouping-0-field' => array_keys($field_labels),
      )
    );
    $form['onelinecal']['cal_height'] = array(
      '#title' => t('Calendar height'),
      '#default_value' => isset($options['cal_height']) ? $options['cal_height'] : '600px',
      '#type' => 'textfield',
      '#dependency' => array(
        'edit-style-options-grouping-0-field' => array_keys($field_labels),
      )
    );
    $form['onelinecal']['cal_width'] = array(
      '#title' => t('Calendar width'),
      '#default_value' => isset($options['cal_width']) ? $options['cal_width'] : '100%',
      '#type' => 'textfield',
      '#dependency' => array(
        'edit-style-options-grouping-0-field' => array_keys($field_labels),
      )
    );
    $form['onelinecal']['header_columns'] = array(
      '#title' => t('Add additional columns to header.'),
      '#default_value' => isset($options['header_columns']) ? $options['header_columns'] : '0',
      '#type' => 'textfield',
      '#dependency' => array(
        'edit-style-options-grouping-0-field' => array_keys($field_labels),
      )
    );
    $form['onelinecal']['scol_grouping'] = array(
      '#title' => t('Enable grouping by second column'),
      '#default_value' => isset($options['scol_grouping']) ? $options['scol_grouping'] : 0,
      '#type' => 'checkbox',
      '#dependency' => array(
        'edit-style-options-grouping-0-field' => array_keys($field_labels),
      )
    );
    $form['onelinecal']['gr_html'] = array(
      '#title' => t('Groups - Inner HTML'),
      '#description' => t('Enter html inside grouped row (right table).'), 
      '#default_value' => isset($options['gr_html']) ? $options['gr_html'] : '&nbsp;',
      '#type' => 'textfield',
      '#dependency' => array(
        'edit-style-options-onelinecal-scol-grouping' => array(1),
      )
    );
    $form['onelinecal']['fixed_column'] = array(
      '#title' => t('Fix first column'),
      '#default_value' => isset($options['fixed_column']) ? $options['fixed_column'] : 0,
      '#type' => 'checkbox',
      '#dependency' => array(
        'edit-style-options-grouping-0-field' => array_keys($field_labels),
      )
    );
    $form['onelinecal']['fixed_column_width'] = array(
      '#title' => t('Fixed column width'),
      '#default_value' => isset($options['fixed_column_width']) ? $options['fixed_column_width'] : '15',
      '#type' => 'textfield',
      '#dependency' => array(
        'edit-style-options-onelinecal-fixed-column' => array(1),
      )
    );
  }

  /**
   * Add grouping+onelinecal validation.
   * @see calendar_plugin_style::options_validate()
   * @see views_plugin_style::options_validate()
   */
  function options_validate(&$form, &$form_state) {
    parent::options_validate($form, $form_state);
    // Don't run validation on style plugins without the grouping setting.
    if (isset($form_state['values']['style_options']['grouping'])) {
      // Don't save grouping and  if no field is specified.
      foreach ($form_state['values']['style_options']['grouping'] as $index => $grouping) {
        if (empty($grouping['field'])) {
          unset($form_state['values']['style_options']['grouping'][$index]);
        }
      }
      //If grouping isn't set, turn off al one line calendat settings
      if (empty($form_state['values']['style_options']['grouping'])) {
        foreach ($form_state['values']['style_options']['onelinecal'] as $index => $value) {
          unset($form_state['values']['style_options']['onelinecal'][$index]);
        }
      }
    }
  }
  
  /**
   * @see calendar_plugin_style::render()
   */
  function render() {
    if (empty($this->row_plugin) || !$this->has_calendar_row_plugin()) {
      debug('calendar_plugin_style: The calendar row plugin is required when using the calendar style, but it is missing.');
      return;
    }
    if (!$argument = $this->date_argument_handler()) {
      debug('calendar_plugin_style: A date argument is required when using the calendar style, but it is missing or is not using the default date.');
      return;
    }

    // There are date arguments that have not been added by Date Views.
    // They will be missing the information we would need to render the field.
    if (empty($argument->min_date)) {
      return;
    }
    // Add information from the date argument to the view.
    $this->date_info->granularity = $this->granularity();
    $this->date_info->calendar_type = $this->options['calendar_type'];
    $this->date_info->date_arg = $argument->argument;
    $this->date_info->year = date_format($argument->min_date, 'Y');
    $this->date_info->month = date_format($argument->min_date, 'n');
    $this->date_info->day = date_format($argument->min_date, 'j');
    $this->date_info->week = date_week(date_format($argument->min_date, DATE_FORMAT_DATE));
    $this->date_info->date_range = $argument->date_range;
    $this->date_info->min_date = $argument->min_date;
    $this->date_info->max_date = $argument->max_date;
    $this->date_info->limit = $argument->limit;
    $this->date_info->url = $this->view->get_url();
    $this->date_info->min_date_date = date_format($this->date_info->min_date, DATE_FORMAT_DATE);
    $this->date_info->max_date_date = date_format($this->date_info->max_date, DATE_FORMAT_DATE);
    $this->date_info->forbid = isset($argument->forbid) ? $argument->forbid : FALSE;
    //We need to calc number of weeks now, so we can render header,
    //such number of times as number of weeks.
    //Min Date in current month
    $wMinDate = $this->date_info->min_date->format('W');
    //Max Date in current month
    $wMaxDate = $this->date_info->max_date->format('W');
    //Number of weeks in year
    $wInYear = date("W", mktime(0, 0, 0, 12, 28, $this->date_info->year));
    //Set Number of week in month. While we counting difference between weeks numbers,
    //we lose 1 week, so "plus one" below appears.
    $this->weekcount = ($wMaxDate - $wMinDate) > 0 ? ($wMaxDate - $wMinDate) + 1 : ($wInYear + ($wMaxDate - $wMinDate)) + 1;
    // Add calendar style information to the view.
    $this->date_info->calendar_popup = $this->display->handler->get_option('calendar_popup');
    $this->date_info->style_name_size = $this->options['name_size'];
    $this->date_info->mini = $this->options['mini'];
    $this->date_info->style_with_weekno = $this->options['with_weekno'];
    $this->date_info->style_multiday_theme = $this->options['multiday_theme'];
    $this->date_info->style_theme_style = $this->options['theme_style'];
    $this->date_info->style_max_items = $this->options['max_items'];
    $this->date_info->style_max_items_behavior = $this->options['max_items_behavior'];
    
    if (!empty($this->options['groupby_times_custom'])) {
      $this->date_info->style_groupby_times = explode(',', $this->options['groupby_times_custom']);
    } else {
      $this->date_info->style_groupby_times = calendar_groupby_times($this->options['groupby_times']);
    }
    
    $this->date_info->style_groupby_field = $this->options['groupby_field'];
    // @TODO make this an option setting.
    $this->date_info->style_show_empty_times = !empty($this->options['groupby_times_custom']) ? TRUE : FALSE;
    // Set up parameters for the current view that can be used by the row plugin.
    $display_timezone = date_timezone_get($this->date_info->min_date);
    $this->date_info->display_timezone = $display_timezone;
    $this->date_info->display_timezone_name = timezone_name_get($display_timezone);
    $date = clone ($this->date_info->min_date);
    date_timezone_set($date, $display_timezone);
    $this->date_info->min_zone_string = date_format($date, DATE_FORMAT_DATE);
    $date = clone ($this->date_info->max_date);
    date_timezone_set($date, $display_timezone);
    $this->date_info->max_zone_string = date_format($date, DATE_FORMAT_DATE);

    // Invoke the row plugin to massage each result row into calendar items.
    // @TODO Because of using 'grouping', we render each row like a full calendar that take a lot of time.
    // Also, big lose of speed is caused by rendering each field, and if there is a lot of result's we get 
    // really big latency in page load, about 30 secs.
    $sets = $this->render_grouping($this->view->result, $this->options['grouping'], TRUE);
    //Render each group in - <tr> - full month view.
    $rows = $this->render_grouping_sets($sets, 0);
    
    //Make header from week headers. 
    $weekheader = calendar_week_header($this->view);
    $header = $weekheader;
    //We must combine weekheaders as manu times as number of weeks in month.
    //To get appropriate header for whole month.
    for ($i = 0; $i < $this->weekcount - 1; $i++) {
      $header = array_merge($weekheader, $header);
    }
    //Variable $this->datebox, schould be already set, while passing through calendar_build_moth().
    //Adding day number to header cell.
    foreach ($this->datebox as $index => $value) {
      $headercell =& $header[$index]['data'];
      $headercell = '<div class="onelinecal-week-day">'.$headercell.'</div><div class="onelinecal-day-number">'.$value.'</div>';
    }

    //Add empty column if grouping enabled
    if (!empty($this->options['grouping'])){
      array_unshift($header, array('header' => TRUE, 'class' => 'days onelinecal-groupcolumn', 'data' => '', 'header_id' => ''));
      if (!empty($this->options['onelinecal']['header_columns'])) {
        //Add addition columns if set by user.
        $colnumb = $this->options['onelinecal']['header_columns'];
        for ($i = 0; $i<$colnumb; $i++) {
          array_unshift($header, array('header' => TRUE, 'class' => 'days onelinecal-groupcolumn', 'data' => '', 'header_id' => ''));
        }
      }
    }
    //Combine header and rows
    $caltable = array($header, array('data' => $rows));

    $output = theme($this->theme_functions(), array(
      'view' => $this->view,
      'options' => $this->options,
      'rows' => $caltable,
      'grouping' => $this->options['grouping'],
      ));
    
    return $output;
  }

  /**
   * Render the grouping sets. This function is recursive. It is called back inside
   * template_preprocess_onelinecal_view_grouping.
   *
   * @param $sets
   *   Array containing the grouping sets to render.
   * @param $level
   *   Integer indicating the hierarchical level of the grouping.
   * @param $groupingcolumn
   *   By default this function will return grouped rows, but if it is
   *   set to TRUE, it will return rows of group titles.
   *   
   * @return string
   *   Rendered output of given grouping sets.
   *
   * @see template_preprocess_onelinecal_view_grouping().
   */
  function render_grouping_sets($sets, $level = 0/*, $groupingcolumn = FALSE*/) {
    $output = '';
    $data = '';

    foreach ($sets as $set) {
      $row = reset($set['rows']);
      // Send the sorted rows to the right theme for this type of calendar.
      $this->definition['theme'] = 'onelinecal_'.$this->options['calendar_type']; // onelinecal_moth

      // Render as a grouping set.
      if (is_array($row) && isset($row['group'])) {
       /* if ($groupingcolumn) {
          dpm($set['group']);
          $data .= theme(views_theme_functions('onelinecal_view_grouping_table', $this->view, $this->display), array(
            'view' => $this->view,
            'grouping' => $this->options['grouping'][$level],
            'grouping_level' => $level,
            'rows' => $set['rows'],
            'columnsnumber' => $this->weekcount * 7 + 1,
            // for the first row were weekcount not ready yet
            'title' => $set['group']));
        } else {*/
          $data .= theme(views_theme_functions('onelinecal_view_grouping', $this->view, $this->display), array(
            'view' => $this->view,
            'grouping' => $this->options['grouping'][$level],
            'grouping_level' => $level,
            'rows' => $set['rows'],
            'columnsnumber' => $this->weekcount * 7 + 1,
            // for the first row were weekcount not ready yet
            'title' => $set['group']));
        //}
      }
      // Render as a record set.
      else {
        /*if ($groupingcolumn) {
          $data .= '<tr><td>'.$set['group'].'</td></tr>';

        } else {*/

          // Gather the row items into an array grouped by date and time.
          $items = array();
          foreach ($set['rows'] as $row_index => $rowdata) {
            $this->view->row_index = $row_index;
            // Let views render fields the way it thinks they should look before we start massaging them.
            $rows = $this->row_plugin->render($rowdata);

            foreach ($rows as $key => $item) {
              $item->granularity = $this->date_info->granularity;
              //$rendered_fields = array();
              $item_start = date_format($item->calendar_start_date, DATE_FORMAT_DATE);
              $item_end = date_format($item->calendar_end_date, DATE_FORMAT_DATE);
              $time_start = date_format($item->calendar_start_date, 'H:i:s');
              $item->rendered_fields = $this->rendered_fields[$row_index];
              $items[$item_start][$time_start][] = $item;
            }
          }
          ksort($items);
          
          $this->curday = clone ($this->date_info->min_date);
          // We reassign items each time, so they will be normally rendered by
          // rest calendar parent's function's
          $this->items = $items;

          // Set method for the granularity of the display.
          $oneline = $this->calendar_build_month();
          //Place each month in <tr>
          $data .= theme('onelinecal_month_row', array(
            'inner' => $oneline,
            //'class' => 'date-box',
            //'iehint' => $iehint,
            'grouping_level' => $level,
            'title' => $set['group'],
          ));

        }
      //}

    }

    unset($this->view->row_index);
    return $data;
  }
  /*
   * /**
   *    * @param $groupingcolumn
   *   By default this function will return grouped rows, but if it is
   *   set to TRUE, it will return rows of group titles.
   *   */
  /*
   function render_grouping_sets($sets, $level = 0, $groupingcolumn = FALSE) {
   $output = '';
   $data = '';

   foreach ($sets as $set) {
   $row = reset($set['rows']);
   // Send the sorted rows to the right theme for this type of calendar.
   $this->definition['theme'] = 'onelinecal_'.$this->options['calendar_type']; // onelinecal_moth

   // Render as a grouping set.
   do {
   $groups .= '<td>'.$row['group'].'</td>';
   $row = reset($set['rows']);
   } while (is_array($row) && isset($row['group']));

   // Render as a record set.
   // Group by date and sort items
   $items = array();
   foreach ($set['rows'] as $row_index => $rowdata) {
   $this->view->row_index = $row_index;
   $rows = $this->row_plugin->render($rowdata);

   foreach ($rows as $key => $item) {
   $item->granularity = $this->date_info->granularity;
   //$rendered_fields = array();
   $item_start = date_format($item->calendar_start_date, DATE_FORMAT_DATE);
   $item_end = date_format($item->calendar_end_date, DATE_FORMAT_DATE);
   $time_start = date_format($item->calendar_start_date, 'H:i:s');
   $item->rendered_fields = $this->rendered_fields[$row_index];
   $items[$item_start][$time_start][] = $item;
   }
   }
   ksort($items);
   // Ground and sort items

   $this->curday = clone ($this->date_info->min_date);
   // We reassign items each time, so they will be normally rendered by
   // rest calendar parent's function's
   $this->items = $items;

   // Retrieve the results array using a the right method for the granularity of the display.
   switch ($this->options['calendar_type']) {
   case 'month':
   $oneline = $this->calendar_build_month();
   break;
   }

   $data .= theme('onelinecal_month_row', array(
   'inner' => $oneline,
   //'class' => 'date-box',
   //'iehint' => $iehint,
   'grouping_level' => $level,
   'title' => $set['group'],
   ));

   }

   unset($this->view->row_index);
   return $data;
   }*/

  /**
   * Build one month.
   * 
   * We need to override calendar_plugin_style::calendar_build_month here,
   * case we want to set datebox propertie. Also we delete rendering each month in row (<tr>),
   * because full calendar will be wrapped in <tr> tag in render_grouping_sets function. 
   * 
   * @see render_grouping_sets().
   */
  function calendar_build_month() {
    $translated_days = date_week_days_ordered(date_week_days(TRUE));
    $month = date_format($this->curday, 'n');
    $curday_date = date_format($this->curday, DATE_FORMAT_DATE);
    $weekdays = calendar_untranslated_days($this->items, $this->view);
    date_modify($this->curday, '-'.strval(date_format($this->curday, 'j') - 1).' days');

    $row = ''; // String of whole month
    $datebox = array(); //Dates of the month

    do {

      $init_day = clone ($this->curday);
      $today = date_format(date_now(date_default_timezone()), DATE_FORMAT_DATE);
      $month = date_format($this->curday, 'n');
      $week = date_week($curday_date);
      $first_day = variable_get('date_first_day', 0);

      $week_rows = $this->calendar_build_week(TRUE);
      //$this->weekcount++;

      $multiday_buckets = $week_rows['multiday_buckets'];
      $singleday_buckets = $week_rows['singleday_buckets'];
      $total_rows = $week_rows['total_rows'];

      // Theme each row
      $output = "";
      $final_day = clone ($this->curday);

      $iehint = 0;
      $max_multirow_cnt = 0;
      $max_singlerow_cnt = 0;

      for ($i = 0; $i < intval($total_rows + 1); $i++) {
        $inner = "";

        // If we're displaying the week number, add it as the
        // first cell in the week.
        if ($i == 0 && !empty($this->date_info->style_with_weekno) && !in_array($this->date_info->granularity, array('day', 'week'))) {
          $path = calendar_granularity_path($this->view, 'week');
          if (!empty($path)) {
            $url = $path.'/'.$this->date_info->year.'-W'.$week;
            $weekno = l($week, $url, array('query' => !empty($this->date_info->append) ? $this->date_info->append : ''));
          } else {
            // Do not link week numbers, if Week views are disabled.
            $weekno = $week;
          }
          $item = array(
            'entry' => $weekno,
            'colspan' => 1,
            'rowspan' => $total_rows + 1,
            'id' => $this->view->name.'-weekno-'.$curday_date,
            'class' => 'week',
          );
          $inner .= theme('calendar_month_col', array('item' => $item));
        }

        $this->curday = clone ($init_day);

        // move backwards to the first day of the week
        $day_wday = date_format($this->curday, 'w');
        date_modify($this->curday, '-'.strval((7 + $day_wday - $first_day) % 7).' days');

        for ($wday = 0; $wday < 7; $wday++) {

          $curday_date = date_format($this->curday, DATE_FORMAT_DATE);
          $class = strtolower($weekdays[$wday]);
          $item = NULL;
          $in_month = !($curday_date < $this->date_info->min_date_date || $curday_date > $this->date_info->max_date_date || date_format($this->curday, 'n') != $month);

          // Add the datebox
          if ($i == 0) {
            $variables = array(
              'date' => $curday_date,
              'view' => $this->view,
              'items' => $this->items,
              'selected' => $in_month ? count($multiday_buckets[$wday]) + count($singleday_buckets[$wday]) : FALSE,
            );
            $item = array(
              'entry' => theme('calendar_datebox', $variables),
              'colspan' => 1,
              'rowspan' => 1,
              'class' => 'date-box',
              'date' => $curday_date,
              'id' => $this->view->name.'-'.$curday_date.'-date-box',
              'header_id' => $translated_days[$wday],
              'day_of_month' => $this->curday->format('j'),
            );
            $item['class'] .= ($curday_date == $today && $in_month ? ' today' : '').($curday_date < $today ? ' past' : '').($curday_date > $today ? ' future' : '');
            if (empty($this->datebox)) // Creating array of dates in month, will place them in header later
              $datebox[] = $item['day_of_month'];
          } else {
            $index = $i - 1;
            $multi_count = count($multiday_buckets[$wday]);

            // Process multiday buckets first.  If there is a multiday-bucket item in this row...
            if ($index < $multi_count) {
              // If this item is filled with either a blank or an entry...
              if ($multiday_buckets[$wday][$index]['filled']) {

                // Add item and add class
                $item = $multiday_buckets[$wday][$index];
                $item['class'] = 'multi-day';
                $item['date'] = $curday_date;

                // Is this an entry?
                if (!$multiday_buckets[$wday][$index]['avail']) {

                  // If the item either starts or ends on today,
                  // then add tags so we can style the borders
                  if ($curday_date == $today && $in_month) {
                    $item['class'] .= ' starts-today';
                  }

                  // Calculate on which day of this week this item ends on..
                  $end_day = clone ($this->curday);
                  $span = $item['colspan'] - 1;
                  date_modify($end_day, '+'.$span.' day');
                  $endday_date = date_format($end_day, DATE_FORMAT_DATE);

                  // If it ends today, add class
                  if ($endday_date == $today && $in_month) {
                    $item['class'] .= ' ends-today';
                  }
                }
              }

              // If this is an actual entry, add classes regarding the state of the
              // item
              if ($multiday_buckets[$wday][$index]['avail']) {
                $item['class'] .= ' '.$wday.' '.$index.' no-entry '.($curday_date == $today && $in_month ? ' today' : '').($curday_date < $today ? ' past' : '').($curday_date > $today ? ' future' : '');
              }

              // Else, process the single day bucket - we only do this once per day
              } elseif ($index == $multi_count) {
              $single_day_cnt = 0;
              // If it's empty, add class
              if (count($singleday_buckets[$wday]) == 0) {
                $single_days = "&nbsp;";
                if ($max_multirow_cnt == 0) {
                  $class = ($multi_count > 0) ? 'single-day no-entry noentry-multi-day' : 'single-day no-entry';
                } else {
                  $class = 'single-day';
                }
              } else {
                $single_days = "";
                foreach ($singleday_buckets[$wday] as $day) {
                  foreach ($day as $event) {
                    $single_day_cnt++;
                    $single_days .= (isset($event['more_link'])) ? '<div class="calendar-more">'.$event['entry'].'</div>' : $event['entry'];
                  }
                }
                $class = 'single-day';
              }

              $rowspan = $total_rows - $index;
              // Add item...
              $item = array(
                'entry' => $single_days,
                'colspan' => 1,
                'rowspan' => $rowspan,
                'class' => $class,
                'date' => $curday_date,
                'id' => $this->view->name.'-'.$curday_date.'-'.$index,
                'header_id' => $translated_days[$wday],
                'day_of_month' => $this->curday->format('j'),
              );

              // Hack for ie to help it properly space single day rows
              if ($rowspan > 1 && $in_month && $single_day_cnt > 0) {
                $max_multirow_cnt = max($max_multirow_cnt, $single_day_cnt);
              } else {
                $max_singlerow_cnt = max($max_singlerow_cnt, $single_day_cnt);
              }

              // If the singlerow is bigger than the multi-row, then null out
              // ieheight - I'm estimating that a single row is twice the size of
              // multi-row.  This is really the best that can be done with ie
              if ($max_singlerow_cnt >= $max_multirow_cnt || $max_multirow_cnt <= $multi_count / 2) {
                $iehint = 0;
              } elseif ($rowspan > 1 && $in_month && $single_day_cnt > 0) {
                $iehint = max($iehint, $rowspan - 1); // How many rows to adjust for?
                }

              // Set the class
              $item['class'] .= ($curday_date == $today && $in_month ? ' today' : '').($curday_date < $today ? ' past' : '').($curday_date > $today ? ' future' : '');

            }
          }

          // If there isn't an item, then add empty class
          if ($item != NULL) {
            if (!$in_month) {
              $item['class'] .= ' empty';
            }
            // Style this entry - it will be a <td>.
            $inner .= theme('calendar_month_col', array('item' => $item));
          }

          date_modify($this->curday, '+1 day');
        }
        //We overriding output, cause we need only last set, first set is dateboxes for cells.
        $output = $inner;
      } // End foreach

      $this->curday = $final_day;
      //Pasting together weeks
      $row = $row.$output; 
      // Add the row into the row array....
      $curday_date = date_format($this->curday, DATE_FORMAT_DATE);
      $curday_month = date_format($this->curday, 'n');
    } while ($curday_month == $month && $curday_date <= $this->date_info->max_date_date);

    if (empty($this->datebox ))
      $this->datebox = $datebox;

    return $row;
  }

  /**
   * Build one week row.

   function calendar_build_week($check_month = FALSE) {
   $curday_date = date_format($this->curday, DATE_FORMAT_DATE);
   $weekdays = calendar_untranslated_days($this->items, $this->view);
   $month = date_format($this->curday, 'n');
   $first_day = variable_get('date_first_day', 0);

   // Set up buckets
   $total_rows = 0;
   $multiday_buckets = array(array(), array(), array(), array(), array(), array(), array());
   $singleday_buckets = array(array(), array(), array(), array(), array(), array(), array());

   // move backwards to the first day of the week
   $day_wday = date_format($this->curday, 'w');
   date_modify($this->curday, '-'.strval((7 + $day_wday - $first_day) % 7).' days');
   $curday_date = date_format($this->curday, DATE_FORMAT_DATE);

   for ($i = 0; $i < 7; $i++) {
   if ($check_month && ($curday_date < $this->date_info->min_date_date || $curday_date > $this->date_info->max_date_date || date_format($this->curday, 'n') != $month)) {
   $class = strtolower($weekdays[$i]).' empty';
   $singleday_buckets[$i][][] = array(
   'entry' => theme('onelinecal_empty_day', array(
   'curday' => $curday_date,
   'view' => $this->view,
   )),
   'item' => NULL
   );
   } else {
   $this->calendar_build_week_day($i, $multiday_buckets, $singleday_buckets);
   }
   $total_rows = max(count($multiday_buckets[$i]) + 1, $total_rows);
   date_modify($this->curday, '+1 day');
   $curday_date = date_format($this->curday, DATE_FORMAT_DATE);
   }

   $rows = array(
   'multiday_buckets' => $multiday_buckets,
   'singleday_buckets' => $singleday_buckets,
   'total_rows' => $total_rows);

   return $rows;
   }
   */
  /**
   * Build the contents of a single day for the $rows results.

   function calendar_build_week_day($wday, &$multiday_buckets, &$singleday_buckets) {
   $curday_date = date_format($this->curday, DATE_FORMAT_DATE);
   $max_events = $this->date_info->calendar_type == 'month' && !empty($this->date_info->style_max_items) ? $this->date_info->style_max_items : 0;
   $hide = !empty($this->date_info->style_max_items_behavior) ? ($this->date_info->style_max_items_behavior == 'hide') : FALSE;
   $multiday_theme = !empty($this->date_info->style_multiday_theme) && $this->date_info->style_multiday_theme == '1';
   $first_day = variable_get('date_first_day', 0);
   $cur_cnt = 0;
   $total_cnt = 0;
   $ids = array();

   // If we are hiding, count before processing further
   if ($max_events != CALENDAR_SHOW_ALL) {
   foreach ($this->items as $date => $day) {
   if ($date == $curday_date) {
   foreach ($day as $time => $hour) {
   foreach ($hour as $key => $item) {
   $total_cnt++;
   $ids[] = $item->date_id;
   }
   }
   }
   }
   }

   // If we haven't already exceeded the max or we'll showing all, then process the items
   if ($max_events == CALENDAR_SHOW_ALL || !$hide || $total_cnt < $max_events) {
   // Count currently filled items
   foreach ($multiday_buckets[$wday] as $bucket) {
   if (!$bucket['avail']) {
   $cur_cnt++;
   }
   }
   foreach ($this->items as $date => $day) {
   if ($date == $curday_date) {
   ksort($day);
   foreach ($day as $time => $hour) {
   foreach ($hour as $key => $item) {
   $all_day = $item->calendar_all_day;

   // Parse out date part
   $start_ydate = date_format($item->date_start, DATE_FORMAT_DATE);
   $end_ydate = date_format($item->date_end, DATE_FORMAT_DATE);
   $cur_ydate = date_format($this->curday, DATE_FORMAT_DATE);

   $is_multi_day = ($start_ydate < $cur_ydate || $end_ydate > $cur_ydate);

   // Does this event span multi-days?
   if ($multiday_theme && ($is_multi_day || $all_day)) {

   // Remove multiday items from the total count. We can't hide them or they will break.
   $total_cnt--;

   // If this the first day of the week, or is the start date of the multi-day event,
   // then record this item, otherwise skip over
   $day_no = date_format($this->curday, 'd');
   if ($wday == 0 || $start_ydate == $cur_ydate || ($this->date_info->granularity == 'month' && $day_no == 1) || ($all_day && !$is_multi_day)) {
   // Calculate the colspan for this event

   // If the last day of this event exceeds the end of the current month or week,
   // truncate the remaining days
   $diff = $this->curday->difference($this->date_info->max_date, 'days');
   $remaining_days = ($this->date_info->granularity == 'month') ? min(6 - $wday, $diff) : $diff - 1;
   // The bucket_cnt defines the colspan.  colspan = bucket_cnt + 1
   $days = $this->curday->difference($item->date_end, 'days');
   $bucket_cnt = max(0, min($days, $remaining_days));

   // See if there is an available slot to add an event.  This will allow
   // an event to precede a row filled up by a previous day event
   $avail = FALSE;
   $bucket_index = count($multiday_buckets[$wday]);
   for ($i = 0; $i < $bucket_index; $i++) {
   if ($multiday_buckets[$wday][$i]['avail']) {
   $bucket_index = $i;
   break;
   }
   }

   // Add continuation attributes
   $item->continuation = ($item->date_start < $this->curday);
   $item->continues = ($days > $bucket_cnt);
   $item->is_multi_day = TRUE;

   // Assign the item to the available bucket
   $multiday_buckets[$wday][$bucket_index] = array(
   'colspan' => $bucket_cnt + 1,
   'rowspan' => 1,
   'filled' => TRUE,
   'avail' => FALSE,
   'all_day' => $all_day,
   'item' => $item,
   'wday' => $wday,
   'entry' => theme('onelinecal_item', array('view' => $this->view, 'rendered_fields' => $item->rendered_fields, 'item' => $item)),
   );

   // Block out empty buckets for the next days in this event for this week
   for ($i = 0; $i < $bucket_cnt; $i++) {
   $bucket =& $multiday_buckets[$i + $wday + 1];
   $bucket_row_count = count($bucket);
   $row_diff = $bucket_index - $bucket_row_count;

   // Fill up the preceding buckets - these are available for future
   // events
   for ($j = 0; $j < $row_diff; $j++) {
   $bucket[($bucket_row_count + $j)] = array(
   'entry' => '&nbsp;',
   'colspan' => 1,
   'rowspan' => 1,
   'filled' => TRUE,
   'avail' => TRUE,
   'wday' => $wday,
   'item' => NULL
   );
   }
   $bucket[$bucket_index] = array(
   'filled' => FALSE,
   'avail' => FALSE
   );
   }
   }
   } elseif ($max_events == CALENDAR_SHOW_ALL || $cur_cnt < $max_events) {
   $cur_cnt++;
   // Assign to single day bucket
   $singleday_buckets[$wday][$time][] = array(
   'entry' => theme('onelinecal_item', array('view' => $this->view, 'rendered_fields' => $item->rendered_fields, 'item' => $item)),
   'item' => $item,
   'colspan' => 1,
   'rowspan' => 1,
   'filled' => TRUE,
   'avail' => FALSE,
   'wday' => $wday,
   );
   }

   }
   }
   }
   }
   }

   // Add a more link if necessary
   if ($max_events != CALENDAR_SHOW_ALL && $total_cnt > 0 && $cur_cnt < $total_cnt) {
   $entry = theme('onelinecal_'.$this->date_info->calendar_type.'_multiple_entity', array(
   'curday' => $curday_date,
   'count' => $total_cnt,
   'view' => $this->view,
   'ids' => $ids,
   ));
   if (!empty($entry)) {
   $singleday_buckets[$wday][][] = array(
   'entry' => $entry,
   'more_link' => TRUE,
   'item' => NULL
   );
   }
   }
   }
   */
  /**
   * Build the contents of a single day for the $rows results.

   function calendar_build_day() {
   $curday_date = date_format($this->curday, DATE_FORMAT_DATE);
   $selected = FALSE;
   $max_events = !empty($this->date_info->style_max_items) ? $this->date_info->style_max_items : 0;
   $ids = array();
   $inner = array();
   $all_day = array();
   $empty = '';
   $link = '';
   $count = 0;
   foreach ($this->items as $date => $day) {
   if ($date == $curday_date) {
   $count = 0;
   $selected = TRUE;
   ksort($day);
   foreach ($day as $time => $hour) {
   foreach ($hour as $key => $item) {
   $count++;
   if (isset($item->type)) {
   $ids[$item->type] = $item;
   }
   if (empty($this->date_info->mini) && ($max_events == CALENDAR_SHOW_ALL || $count <= $max_events || ($count > 0 && $max_events == calendar_HIDE_ALL))) {
   if ($item->calendar_all_day) {
   $item->is_multi_day = TRUE;
   $all_day[] = $item;
   } else {
   $key = date_format($item->calendar_start_date, 'H:i:s');
   $inner[$key][] = $item;
   }
   }
   }
   }
   }
   }
   ksort($inner);

   if (empty($inner) && empty($all_day)) {
   $empty = theme('onelinecal_empty_day', array('curday' => $curday_date, 'view' => $this->view));
   }
   // We have hidden events on this day, use the theme('onelinecal_multiple_') to show a link.
   if ($max_events != CALENDAR_SHOW_ALL && $count > 0 && $count > $max_events && $this->date_info->calendar_type != 'day' && !$this->date_info->mini) {
   if ($this->date_info->style_max_items_behavior == 'hide' || $max_events == calendar_HIDE_ALL) {
   $all_day = array();
   $inner = array();
   }
   $link = theme('onelinecal_'.$this->date_info->calendar_type.'_multiple_node', array(
   'curday' => $curday_date,
   'count' => $count,
   'view' => $this->view,
   'ids' => $ids,
   ));
   }

   $content = array(
   'date' => $curday_date,
   'datebox' => theme('onelinecal_datebox', array(
   'date' => $curday_date,
   'view' => $this->view,
   'items' => $this->items,
   'selected' => $selected,
   )),
   'empty' => $empty,
   'link' => $link,
   'all_day' => $all_day,
   'items' => $inner,
   );
   return $content;
   }
   */
}
