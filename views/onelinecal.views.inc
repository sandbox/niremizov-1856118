<?php

/**
 * Implementation of hook_views_plugins
 */
function onelinecal_views_plugins() {
  $views_path = drupal_get_path('module', 'views');
  $module_path = drupal_get_path('module', 'onelinecal');
  module_load_include('inc', 'onelinecal', 'theme/theme');

  // Limit these plugins to base tables that represent entities.
  $base = array_keys(date_views_base_tables());

  $data = array(
    'module' => 'onelinecal', // This just tells our themes are elsewhere.

    'style' => array(
      'onelinecal_style' => array(
        'title' => t('One Line Calendar'),
        'help' => t('Present view results as a onelinecal.'),
        'handler' => 'onelinecal_plugin_style',
        'path' => "$module_path/views",
        'theme' => 'onelinecal_style',
        'theme file' => 'theme.inc',
        'theme path' => "$module_path/theme",
        'additional themes' => array(
          'onelinecal_mini' => 'style',
          'onelinecal_day' => 'style',
          'onelinecal_week' => 'style',
          'onelinecal_month' => 'style',
          'onelinecal_year' => 'style',
          'onelinecal_day_overlap' => 'style',
          'onelinecal_week_overlap' => 'style',
        ),
        'uses fields' => TRUE,
        'uses grouping' => TRUE,
        'uses row plugin' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
        'even empty' => TRUE,
        'base' => $base,
      ),
    ),
  );
  return $data;
}

