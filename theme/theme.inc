<?php

/**
 * @file
 * Theme functions for the onelinecal module.
 */

/**
 * Preprocess an RSS feed
 */
function template_preprocess_onelinecal_style(&$vars) {
  global $base_url;
  global $language;

}

/**
 * Display a month view.
 */
function template_preprocess_onelinecal_month(&$vars) {
  //Trigger parent method
  template_preprocess_calendar_month($vars);
  
  $view = $vars['view'];
  $module_path = drupal_get_path('module', 'onelinecal');
  $onelinecalOpt = $view->style_options['onelinecal'];

  drupal_add_css($module_path.'/css/onelinecal.css', 'file');
  //Add Datatables libraries
  drupal_add_js($module_path.'/js/datatables/jquery.dataTables.min.js', 'file');
  drupal_add_js($module_path.'/js/datatables/FixedColumns.min.js', 'file');
  $language = language_default()->language;
  //Post settings to onelinecal.js
  drupal_add_js(array('onelinecal' => array(
    'calHeight' => $onelinecalOpt['cal_height'], 
    'calWidth' => $onelinecalOpt['cal_width'], 
    'fixedColumn' => $onelinecalOpt['fixed_column'],
    'fixedColumnWidth' => $onelinecalOpt['fixed_column_width'],
    'scolGrouping' => $onelinecalOpt['scol_grouping'],
    'translationFile' => $language != 'en' ? base_path().$module_path.'/js/datatables/dataTables.'.$language.'.txt' : '',
    'groupInnerHtml' => $onelinecalOpt['gr_html'])
    ),
    'setting'
  );
  //Add js for styling calendar table
  drupal_add_js($module_path.'/js/onelinecal.js', 'file');
}

/**
 * Implementation of hook_preprocess_calendar_item().

function template_preprocess_onelinecal_item(&$vars) {
  // At the last possible minute we fix the values in rendered_fields so it
  // contains the correct rendered content for the type of item and item display.
  $item = $vars['item'];

  $multiday_hidden = !empty($vars['view']->style_options['multiday_hidden']) ? $vars['view']->style_options['multiday_hidden'] : array();

  if (!empty($item->rendered) && empty($item->is_multi_day)) {
    $vars['rendered_fields'] = array($item->rendered);
  }
  else {
    foreach ($vars['view']->field as $id => $field) {
      if ($field->options['exclude'] || (!empty($item->is_multi_day) && in_array($id, $multiday_hidden))) {
        unset($vars['rendered_fields'][$field->field]);
      }
    }
  }
}
 */
/**
 * Process a single grouping within a view.
 */
function template_preprocess_onelinecal_view_grouping(&$vars) {
  $vars['content'] = $vars['view']->style_plugin->render_grouping_sets($vars['rows'], $vars['grouping_level']);
}

/**
 * Process a single grouping within a view.

function template_preprocess_onelinecal_view_grouping_table(&$vars) {
  $vars['content'] = $vars['view']->style_plugin->render_grouping_sets($vars['rows'], $vars['grouping_level'], TRUE);
} */