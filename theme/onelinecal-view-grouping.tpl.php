<?php
/**
 * @file
 * This template is used to print a single grouping in a onelinecal view.
 *
 * The group is a row that placed above a grouping month line.
 *
 * Variables available:
 * - $view: The view object
 * - $grouping: The grouping instruction.
 * - $grouping_level: Integer indicating the hierarchical level of the grouping.
 * - $rows: The rows contained in this grouping.
 * - $title: The title of this grouping.
 * - $content: The processed content output that will normally be used.
 * - $columnsnumber: Number of columns in row.
 */
?>
<?php //If we have more than one column ?>

<tr>
<td class="onelinecal-groupcolumn" colspan="<?php/* print $columnsnumber */?>"><?php print $title; ?></td>
<?php for ($i=1;$i<$columnsnumber;$i++) :?>
  <td class="onelinecal-groupcolumn" ></td>
<?php endfor ?>
</tr>

<?php print $content; ?>