<?php
/**
 * @file
 * Template to display a row of month.
 * 
 * - $inner: The rendered string of the row's contents.
 * - $title: Title of group, if grouping is enabled.
 */
$attrs = ($class) ? 'class="' . $class . '"': '';
$attrs .= ($iehint > 0) ? ' iehint="' . $iehint . '"' : '';
?>

<?php if ($attrs != ''):?>
<tr <?php print $attrs?>>
<?php else:?>
<tr>
<?php endif;?>
    <?php if (!empty($title)): ?>
      <td><?php print $title; ?></td>
    <?php endif ?>
  <?php print $inner ?>
</tr>
