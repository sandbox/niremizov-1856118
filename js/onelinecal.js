/**
 * @file
 * 
 * Adding vertical and horizontal sroll for calendar table.
 * Also adding grouping bu 2nd column and fixing 1st column if set.
 */
(function($) {
	Drupal.behaviors.onelinecal = {
		attach : function(context, settings) {

			var scloDefault = [];
			var language = {};
			
			if (settings.onelinecal.scolGrouping) {
				//Hide 2nd column if grouping is set.
				scloDefault = [ {
					"bVisible" : false,
					"aTargets" : [ 1 ],
				}, ]
			}
			
			if (settings.onelinecal.translationFile) {
				//Add translation
				language =  {
	            	"sUrl": settings.onelinecal.translationFile,          
	            };
			} 

			var oTable = $('#onelinecal-table').dataTable({
				"sScrollX" : settings.onelinecal.calWidth, // Width
				"sScrollY" : settings.onelinecal.calHeight, // Height
				"bSort" : false,
				"bPaginate" : false,
				"aoColumnDefs" : scloDefault,
	            "oLanguage": language,
	            "fnInitComplete": function(oSettings, json) {  
	            	// Init Fixed column. We cannot initialise fixed column separately, cause of language file. 
	            	// Language is setted after table is initialized, and init of Fixed column after that would force an error.
	    			if (settings.onelinecal.fixedColumn) {
	    				if (settings.onelinecal.scolGrouping) {
	    					new FixedColumns(
	    							oTable,
	    							{
	    								"sLeftWidth" : 'relative',
	    								"iLeftWidth" : settings.onelinecal.fixedColumnWidth,
	    								"fnDrawCallback" : function(left, right) {
	    									//Define redraw function, to group by 2nd column
	    									var oSettings = oTable.fnSettings();
	    									if (oSettings.aiDisplay.length == 0)
	    										return;

	    									var nGroup, nCell, iIndex, sGroup;
	    									var sLastGroup = "", iCorrector = 0;
	    									var nTrs = $('#onelinecal-table tbody tr');
	    									var iColspan = nTrs[0]
	    											.getElementsByTagName('td').length;

	    									for ( var i = 0; i < nTrs.length; i++) {
	    										iIndex = oSettings._iDisplayStart + i;
	    										sGroup = oSettings.aoData[oSettings.aiDisplay[iIndex]]._aData[1];

	    										if (sGroup != sLastGroup) {
	    											// Cell to insert into main table
	    											nGroup = document
	    													.createElement('tr');
	    											nCell = document
	    													.createElement('td');
	    											nCell.colSpan = iColspan;
	    											nCell.className = "onelinecal-groupcolumn";
	    											nCell.innerHTML = settings.onelinecal.groupInnerHtml;
	    											nGroup.appendChild(nCell);
	    											nTrs[i].parentNode.insertBefore(
	    													nGroup, nTrs[i]);
	    											// Cell to insert into the frozen
	    											// columns
	    											nGroup = document
	    													.createElement('tr');
	    											nCell = document
	    													.createElement('td');
	    											nCell.className = "onelinecal-groupcolumn";
	    											// Lets input name of group
	    											nCell.innerHTML = sGroup;
	    											nGroup.appendChild(nCell);
	    											$(nGroup)
	    													.insertBefore(
	    															$(
	    																	'tbody tr:eq('
	    																			+ (i + iCorrector)
	    																			+ ')',
	    																	left.body)[0]);
	    											iCorrector++;
	    											sLastGroup = sGroup;
	    										}
	    									}
	    								}
	    							});
	    				} else {
	    					new FixedColumns(oTable);
	    				}
	    			}
	            }
			});


		}
	}
})(jQuery);
